#!/bin/bash

case $SHELL in
*/zsh)
   setopt aliases
   READCMD='read -k1 -r'
   ;;
*)
   # Presume bash and anything else
   shopt -s expand_aliases
   READCMD='read -n 1 -r'
   ;;
esac


publicDNS=$(curl http://169.254.169.254/latest/meta-data/public-hostname)
shortDNS1=${publicDNS/ec2-}
shortDNS=${shortDNS1/.compute-1.amazonaws.com}
oldDNS1=$(egrep -r "site-url" /opt/atlassian/data/fecru/config.xml)
oldDNS=$(echo "$oldDNS1" | cut -d"e" -f6 | sed 's/^...//' | sed 's/.\{7\}$//')
echo 'This public DNS is ' $publicDNS
echo 'The short DNS is ' $shortDNS
echo 'This is the old short DNS from previous AWS instance' $oldDNS

function replace_dns_initial {
#publicDNS=$(curl http://169.254.169.254/latest/meta-data/public-hostname)
#shortDNS1=${publicDNS/ec2-}
#shortDNS=${shortDNS1/.compute-1.amazonaws.com}
#oldDNS1=$(egrep -r "site-url" /opt/atlassian/data/fecru/config.xml)
#oldDNS=$(echo "$oldDNS1" | cut -d"e" -f6 | sed 's/^...//' | sed 's/.\{7\}$//')
echo 'This public DNS is ' $publicDNS
echo 'The short DNS is ' $shortDNS
echo 'This is the old short DNS from previous AWS instance' $oldDNS
if [ "$1" = "jira" ]
then
    echo "Replacing server URL for" $1
    sed -i -E -e "s/jira.teamsinspace.com/$publicDNS/g" /opt/atlassian/data/$1/database/jiradb-permanent.script
    sed -i -E -e "s/confluence.teamsinspace.com/$publicDNS/g" /opt/atlassian/data/$1/database/jiradb-permanent.script
    sed -i -E -e "s/bitbucket.teamsinspace.com/$publicDNS/g" /opt/atlassian/data/$1/database/jiradb-permanent.script
elif [ "$1" = "confluence" ]
then
    echo "Replacing server URL for" $1
    sed -i -E -e "s/confluence.teamsinspace.com/$publicDNS/g" /opt/atlassian/data/$1/database/confluencedb-permanent.script
    sed -i -E -e "s/confluence.teamsinspace.com/$publicDNS/g" /opt/atlassian/data/$1/logs/atlassian-confluence.log 
#    hexdump -ve '1/1 "%.2X"' /opt/atlassian/data/confluence/database/h2db.h2.db | sed -E -e "s/$oldDNS/$shortDNS/g" | xxd -r -p > /opt/atlassian/data/confluence/database/h2db.h2.db.patched
elif [ "$1" = "bitbucket" ]
then
    echo "Replacing server URL for" $1
    sed -i -E -e "s/bitbucket.teamsinspace.com/$publicDNS/g" /opt/atlassian/data/$1/shared/data/db.script
elif [ "$1" = "bamboo" ]
then
    echo "Replacing server URL for" $1
    sed -i -E -e "s/bamboo.teamsinspace.com/$publicDNS/g" /opt/atlassian/data/$1/database/defaultdb.script
    sed -i -E -e "s/bamboo.teamsinspace.com/$publicDNS/g" /opt/atlassian/data/$1/xml-data/configuration/administration.xml
elif [ "$1" = "fecru" ]
then
    echo "Replacing server URL for" $1
    sed -i -E -e "s/fecru.teamsinspace.com/$publicDNS/g" /opt/atlassian/data/$1/config.xml
else
    echo 'entry did not match any DNS configuration change.  Try "replace_dns_initial jira" as an example.'
fi
}

function replace_dns_update {
echo 'This public DNS is ' $publicDNS
echo 'The short DNS is ' $shortDNS
echo 'This is the old short DNS from previous AWS instance' $oldDNS
if [ "$1" = "jira" ]
then
    echo "Replacing server URL for" $1
    sed -i -E -e "s/ec2-(\b[0-9]{1,3}\-){3}[0-9]{1,3}.compute-1.amazonaws.com\b/$publicDNS/g" /opt/atlassian/data/$1/database/jiradb-permanent.script
elif [ "$1" = "confluence" ]
then
    echo "Replacing server URL for" $1
    sed -i -E -e "s/ec2-(\b[0-9]{1,3}\-){3}[0-9]{1,3}.compute-1.amazonaws.com\b/$publicDNS/g" /opt/atlassian/data/$1/database/confluencedb-permanent.script
    sed -i -E -e "s/ec2-(\b[0-9]{1,3}\-){3}[0-9]{1,3}.compute-1.amazonaws.com\b/$publicDNS/g" /opt/atlassian/data/$1/logs/atlassian-confluence.log
elif [ "$1" = "bitbucket" ]
then
    echo "Replacing server URL for" $1
    sed -i -E -e "s/ec2-(\b[0-9]{1,3}\-){3}[0-9]{1,3}.compute-1.amazonaws.com\b/$publicDNS/g" /opt/atlassian/data/$1/shared/data/db.script
elif [ "$1" = "bamboo" ]
then
    echo "Replacing server URL for" $1
    sed -i -E -e "s/ec2-(\b[0-9]{1,3}\-){3}[0-9]{1,3}.compute-1.amazonaws.com\b/$publicDNS/g" /opt/atlassian/data/$1/database/defaultdb.script
    sed -i -E -e "s/ec2-(\b[0-9]{1,3}\-){3}[0-9]{1,3}.compute-1.amazonaws.com\b/$publicDNS/g" /opt/atlassian/data/$1/xml-data/configuration/administration.xml
elif [ "$1" = "fecru" ]
then
    echo "Replacing server URL for" $1
    sed -i -E -e "s/ec2-(\b[0-9]{1,3}\-){3}[0-9]{1,3}.compute-1.amazonaws.com\b/$publicDNS/g" /opt/atlassian/data/$1/config.xml
else
    echo 'entry did not match any DNS configuration change.  Try "replace_dns_update jira" as an example.'
fi
}

function replaceAll_dns_initial {
    replace_dns_initial jira
    replace_dns_initial confluence
    replace_dns_initial bitbucket
    replace_dns_initial bamboo
    replace_dns_initial fecru
    # cd /opt/atlassian/scripts/ && ./movetopresent.sh
    echo "finished updating all App Links Initially for updating master TIS repo in AWS" $publicDNS
}

function replaceAll_dns_update {
    replace_dns_update jira
    replace_dns_update confluence
    replace_dns_update bitbucket
    replace_dns_update bamboo
    replace_dns_update fecru
    # cd /opt/atlassian/scripts/ && ./movetopresent.sh
    echo "finished updating all App Links for updating AWS Image" $publicDNS
}
